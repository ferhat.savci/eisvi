/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.eisvi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ferhats
 */
public class Worker implements Runnable {

  public boolean enabled = false;

  @Override
  public void run() {
    while (true) {

      Logger.getLogger(Worker.class.getName()).log(Level.INFO, "Wakey wakey!");
      
      if (Main.config.containsKey("service.endpoint") && Main.config.containsKey("smartcard.pin") &&
              (enabled || Boolean.valueOf(Main.config.getProperty("autoSign", "false")))) {

        enabled = false;

        Main.trayMenuIcon.setImage(Main.workingImage);

        fillInWorkQueue();
        processWorkQueue();

        try {
          Thread.sleep(2000L);
        } catch (InterruptedException ex) {
          // nothing to report
        }

        Main.trayMenuIcon.setImage(Main.menuImage);
      }

      synchronized (this) {
        try {
          wait(60000L);
        } catch (Exception ex) {
          // nothing to report
        }
      }
    }
  }

  private BlockingQueue<String> fileOids = new LinkedBlockingQueue<>();

  private void fillInWorkQueue() {
    try {
      String endpoint = Main.config.getProperty("service.endpoint");
      JsonObject json = new JsonObject();
      json.addProperty("tckn", "19531612064");
//            json.addProperty("tckn", "110801");
      URL url = new URL(endpoint + "?cmd=TOBB_signman_query_signing_task_by_tckn&jp=" + json.
              toString());

      InputStream is = url.openConnection().getInputStream();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      copyStream2Stream(is, baos);

      JsonObject resp = (JsonObject) new JsonParser().parse(new String(baos.toByteArray(), "utf-8"));

      resp.get("data").getAsJsonObject().get("oids").getAsJsonArray().forEach(
              new Consumer<JsonElement>() {
        @Override
        public void accept(JsonElement t) {
          String oid = t.getAsString();
          Logger.getLogger(Worker.class.getName()).log(Level.INFO, oid + " imzalanacak.");
          fileOids.offer(oid);
        }
      });
    } catch (Exception ex) {
      Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void processWorkQueue() {
    try {
      while (fileOids.size() > 0) {
        String fileOid = fileOids.take();
        Logger.getLogger(Worker.class.getName()).log(Level.INFO, fileOid + " imzalanıyor.");
        byte[] signed = sign(fileOid);
        uploadSigned(fileOid, signed);
        Logger.getLogger(Worker.class.getName()).log(Level.INFO, fileOid + " imzalandı.");
      }
    } catch (Exception ex) {
      Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void copyStream2Stream(InputStream is, OutputStream os) throws IOException {
    byte[] buffer = new byte[1480]; // MTU -- kesin WAN üzerindeyiz!
    int n;
    do {
      n = is.read(buffer);
      if (n > 0) {
        os.write(buffer, 0, n);
      }
    } while (n > 0);
  }

  private byte[] sign(String oid) throws IOException {
    byte[] result = new byte[4096];
    String endpoint = Main.config.getProperty("service.endpoint");
    JsonObject json = new JsonObject();
    json.addProperty("oid", oid);
    URL url = new URL(endpoint + "?cmd=TOBB_signman_offer_signing_task_by_oid");
    URLConnection uconn = url.openConnection();
    uconn.setDoOutput(true);

    OutputStream os = uconn.getOutputStream();
    os.write("jp=".getBytes("utf-8"));
    os.write(json.toString().getBytes("utf-8"));

    InputStream is = uconn.getInputStream();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    copyStream2Stream(is, baos);

    JsonObject resp = (JsonObject) new JsonParser().parse(new String(baos.toByteArray(), "utf-8"));

    String mimetype = resp.get("data").getAsJsonObject().get("mimetype").getAsString();
    String content = resp.get("data").getAsJsonObject().get("content").getAsString();

    result = Base64.getDecoder().decode(content);

    Logger.getLogger(Worker.class.getName()).
            log(Level.INFO, "Got " + oid + " as " + resp.toString());

    return result;
  }

  private void uploadSigned(String oid, byte[] signed) throws IOException {
    String endpoint = Main.config.getProperty("service.endpoint");
    JsonObject json = new JsonObject();
    json.addProperty("oid", oid);
    json.addProperty("signertckn", "19531612064");
    json.addProperty("signername", "Ahmet Üstün");
    json.addProperty("signedcontent", Base64.getEncoder().encodeToString(signed));
    URL url = new URL(endpoint + "?cmd=TOBB_signman_complete_signing_task_by_oid");
    URLConnection uconn = url.openConnection();
    uconn.setDoOutput(true);

    OutputStream os = uconn.getOutputStream();
    os.write("jp=".getBytes("utf-8"));
    os.write(json.toString().getBytes("utf-8"));

    InputStream is = uconn.getInputStream();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    copyStream2Stream(is, baos);

    JsonObject resp = (JsonObject) new JsonParser().parse(new String(baos.toByteArray(), "utf-8"));
    Logger.getLogger(Worker.class.getName()).log(Level.INFO, resp.toString());
  }
}
