/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.eisvi;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.rmi.RemoteException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.login.LoginException;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import tr.com.cs.atr2prov.ProviderLoader;
import tr.com.cs.sass.RemoteSignerImpl;
import tr.com.cs.sass.RemoteSignerWorkerManager;
import tr.com.cs.sass.TokenEntry;
import tr.com.cs.sass.helper.SunPKCS11Helper;
import tr.com.cs.sass.remote.CertificateSelector;
import tr.com.cs.sass.remote.SubjectSerialNumberCertSelector;
import tr.com.cs.sass.remote.XMLSignProfile;
import tr.com.cs.signer.xml.SignPropsConstants;

/**
 *
 * @author ferhats
 */
public class Main {

    public static Properties config = new Properties();

    public static CheckboxMenuItem autoSignItem = new CheckboxMenuItem("Otomatik imzala");
    public static JDialog configureDialog;

    private static File props;

    private static Worker worker = new Worker();

    private static SystemTray tray;
    public static TrayIcon trayMenuIcon;
    public static Image menuImage;
    public static Image workingImage;

    private static boolean tlsServer = false;

    public static void main(String[] args) throws IOException {
        //Check the SystemTray is supported
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            JOptionPane.showMessageDialog(null, "Sistem 'tray' uygulamaları desteklenmeyen bir platformdasınız.",
                    "Uyumsuz sistem!",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(99);
        }

        // Load the config
        boolean configured = false;

        String homeDir = System.getProperty("user.home");
        File configDir = new File(homeDir, ".eisvi");
        if (!configDir.exists()) {
            configDir.mkdirs();
            Logger.getLogger(Main.class.getName()).log(Level.WARNING,
                    "No configuration directory found for user. Created " + configDir.getAbsolutePath());
        }

        props = new File(configDir, "settings.properties");
        if (props.exists()) {
            try (FileInputStream fis = new FileInputStream(props)) {
                config.load(fis);
                if (config.containsKey("autoSign")) {
                    autoSignItem.setState(Boolean.valueOf(config.getProperty("autoSign")));
                }
                configured = true;
            } catch (Throwable t) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, props.getAbsolutePath(), t);
            }
        } else {
            Logger.getLogger(Main.class.getName()).log(Level.WARNING,
                    "No configuration file found for user. " + props.getAbsolutePath());
        }

        ProviderLoader provlo = ProviderLoader.getInstance();

        try {
            provlo.load();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "İmza cihazına erişimde sorun var.\n" +
                    "Cihazın bilgisayara takılı olduğundan\n" +
                    "ve sürücülerinin kurulu olduğundan emin olunuz.\n" +
                    "\n" +
                    "Bazı sistemlerde USB cihazların görülmesi\n" +
                    "uzun sürebilmektedir. Cihaz takılı ve sürücüler\n" +
                    "kurulmuş ise, birkaç saniye bekleyip\n" +
                    "yeniden deneyiniz.", "Hata!", JOptionPane.ERROR_MESSAGE);
            System.exit(9);
        }

        if (!ProviderLoader.getInstance().getCert().getSubjectDN().getName().equals(config.getProperty("signer.dn"))) {
            JOptionPane.showMessageDialog(null, "İmzacı bilgileri değişti.\n" +
                    "\n" +
                    "Kayıtlı: " + Main.config.getProperty("signer.dn") + "\n" +
                    "\n" +
                    "Cihazdaki: " + ProviderLoader.getInstance().getCert().getSubjectDN().getName() + "\n" +
                    "\n" +
                    "İmzacı bilgileri güncelleniyor, PIN bilgisi geçersiz hale getiriliyor, yeni PIN'inizi ayarlardan giriniz.\n",
                    "Hata!", JOptionPane.ERROR_MESSAGE);
            config.remove("smartcard.pin");
            Map newSignerDn = new HashMap();
            newSignerDn.put("signer.dn", ProviderLoader.getInstance().getCert().getSubjectDN().getName());
            configure(newSignerDn);
        }

        tray = SystemTray.getSystemTray();

        menuImage = loadImage("/icon/trayIcon.png");
        workingImage = loadImage("/icon/threegears.png");

        PopupMenu popup = new PopupMenu();

        // Create pop-up menu components
        MenuItem configureItem = new MenuItem("Ayarlar...");
        configureItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                // create dialog
                configureDialog = new JDialog();
                configureDialog.setAlwaysOnTop(true);
                // put in the configurator panel
                Configurator c = new Configurator();
                configureDialog.add(c);
                // let the dialog resize to its contents
                configureDialog.pack();
                // center dialog on screen
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screenSize = toolkit.getScreenSize();
                int x = (screenSize.width - configureDialog.getWidth()) / 2;
                int y = (screenSize.height - configureDialog.getHeight()) / 2;
                configureDialog.setLocation(x, y);
                // show it
                configureDialog.setVisible(true);
            }
        });

        MenuItem exitItem = new MenuItem("Çık");
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });

        MenuItem signNowItem = new MenuItem("Şimdi imzala!");
        signNowItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                synchronized (worker) {
                    worker.enabled = true;
                    worker.notify();
                }
            }
        });

        autoSignItem.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Map setting = new HashMap();
                setting.put("autoSign", Boolean.toString(autoSignItem.getState()));
                configure(setting);
            }
        });

        popup.add(signNowItem);
        popup.add(autoSignItem);
        popup.addSeparator();
        popup.add(configureItem);
        popup.add(exitItem);

        trayMenuIcon = new TrayIcon(menuImage);
        trayMenuIcon.setPopupMenu(popup);
        trayMenuIcon.setToolTip("Cybersoft yerel e-İmza servisi");
        trayMenuIcon.setImageAutoSize(true);

        try {
            tray.add(trayMenuIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }

        if (!configured) {
            configureItem.getActionListeners()[0].actionPerformed(null);
        }

        if (config.getProperty("smartcard.pin") != null && !config.getProperty("smartcard.pin").isEmpty()) {
            try {
                setupSignatureDevice(provlo);
            } catch (KeyStoreException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Cannot authenticate using PIN.", ex);
                JOptionPane.showMessageDialog(null, "İmza cihazına erişimde sorun var.\n" +
                        "PIN bilgisini doğru girdiğinizden emin olun.\n" +
                        "Çoğu sertifika hizmet sağlayıcısı 3 hatalı PIN girişinden sonra imza cihazını kilitlemektedir.\n" +
                        "Böyle bir durumda sertifika hizmet sağlayıcınızın sağladığı PUK kodu ile kilidi kaldırıp PIN'inizi güncelleyebilirsiniz.",
                        "Hata!", JOptionPane.ERROR_MESSAGE);
            } catch (Throwable ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Thread t = new Thread(worker);
        t.start();

        // "c:\Program Files\Java\jdk1.8.0_121\bin\keytool.exe" -genkey -alias localhosttls -keyalg RSA -keysize 2048 -sigalg SHA256withRSA -validity 7300 -keystore tlscert.jks -storepass "p455w02d" -ext SAN=dns:localhost,ip:127.0.0.1
        // for "localhost"
        HttpServer server = null;

        if (Boolean.parseBoolean(Main.config.getProperty("https.tls"))) {
            server = HttpsServer.create(new InetSocketAddress(33742), 0);
            tlsServer = true;

            SSLContext sslContext = getSslContext(Main.class.getResourceAsStream("/tlscert.jks"), "p455w02d");
            HttpsConfigurator configurator = getConfigurator(sslContext);
            ((HttpsServer) server).setHttpsConfigurator(configurator);
        } else {
            server = HttpServer.create(new InetSocketAddress(33742), 0);
        }

        System.out.println("Server is based on: " + server.getClass().getSimpleName());
        server.createContext("/localsignman").setHandler(new EISVIServer());
        server.start();
    }

    public static void setupSignatureDevice(ProviderLoader provlo) throws HeadlessException, NumberFormatException, KeyStoreException {
        X509Certificate xcert = provlo.getCert();

        Map<String, List<TokenEntry>> certificates = new HashMap<>();

        String device = "signature-device";
        int indexOfSlot = 0;
        String alias = provlo.getAlias();
        String serialNumber = "";
        String subjectDN = xcert.getSubjectDN().getName();

        Pattern p = Pattern.compile("SERIALNUMBER=[0-9]*");
        Matcher m = p.matcher(subjectDN);

        PrintStream logWriter = System.err;

        if (m.find()) {
            serialNumber = m.group().substring("SERIALNUMBER=".length());
            logWriter.println("\t[SERIALNUMBER] (in SERIALNUMBER=) " + serialNumber);
        } else {
            String regex = System.getProperty("csp.serialnumber.regex");
            String sss = System.getProperty("csp.serialnumber.offset");
            int substart;
            if (regex == null || sss == null) {
                regex = "2.5.4.5=#13[0-9a-fA-F]*";
                substart = "2.5.4.5=#13FF".length(); // FF is the byte holding the number of bytes following, e.g., 0a for mali mühür, 0b for NES
            } else {
                substart = Integer.parseInt(sss);
            }

            p = Pattern.compile(regex);
            m = p.matcher(subjectDN);
            if (m.find()) {
                logWriter.println("\t\t[OID REGEX] " + m.group());

                String serialNumberInHex = m.group().substring(substart);

                // this needs an even number of hex digits, try to left pad with 0 if not -- probably will fail
                if (serialNumberInHex.length() % 2 != 0) {
                    logWriter.println(
                            "\t\tOdd number of hex digits in the byte array for the serial number, will left pad with 0. Expect it to fail.");
                    serialNumberInHex = "0" + serialNumberInHex;
                }

                logWriter.println("\t[SERIALNUMBER] (in REGEX) " + serialNumberInHex);
                int nbytes = serialNumberInHex.length() / 2;
                byte[] bytes = new byte[nbytes];

                for (int n = 0; n < nbytes; n++) {
                    String byteStr = serialNumberInHex.substring(n * 2, n * 2 + 2);
                    bytes[n] = Byte.parseByte(byteStr, 16);
                }

                serialNumber = "";
                try {
                    serialNumber = new String(bytes, "utf-8");
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace(logWriter);

                }

                logWriter.println("\t[SERIALNUMBER] (decoded) " + serialNumber);
            } else {
                logWriter.println("\t[SERIALNUMBER] no REGEX match for ('" + regex +
                        "') or 'SERIALNUMBER=' in certificate subject. CERTIFICATE UNUSABLE.");
            }
        }

        TokenEntry te = null;
        te = new TokenEntry(device, indexOfSlot, alias, serialNumber, subjectDN, alias, config.getProperty("smartcard.pin"),
                xcert);
        //device, indexOfSlot, alias, serialNumber, subjectDN, alias, pinCode.get(device), xcert);
        te.setProviderHelper(new SunPKCS11Helper());
        try {
            te.getProviderHelper().load(provlo.getPkcs11Config().get("library"), device, indexOfSlot, config.getProperty(
                    "smartcard.pin"), null);
        } catch (KeyStoreException kse) {
            throw kse;
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Loading SunPKCS11 provider", ex);
            JOptionPane.showMessageDialog(null, "İmza cihazına erişimde sorun var.\n" +
                    "Java akıllı kart kütüphanesi hata veriyor.\n" +
                    "Bilgisayarınızı kapatıp açmayı deneyebilirsiniz.\n" +
                    "\n\nKapatıp açtıktan sonra da çalışmazsa, destek çağrısı açınız.", "Hata!", JOptionPane.ERROR_MESSAGE);
            System.exit(9);
        }

        List<TokenEntry> lte = certificates.get(serialNumber);
        if (lte == null) {
            lte = new ArrayList<>();
            certificates.put(serialNumber, lte);
        }

        lte.add(te);

        RemoteSignerWorkerManager.getInstance().setCertificates(certificates, true);
        RemoteSignerWorkerManager.getInstance().setupHardware();
    }

    public static boolean isTlsServer() {
        return tlsServer;
    }

    private static SSLContext getSslContext(InputStream keyStoreStream, String passPhrase) throws IOException {
        try {
            final KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(keyStoreStream, passPhrase.toCharArray());

            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                System.err.println("Keystore alias: " + aliases.nextElement());
            }

            final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keystore, passPhrase.toCharArray());
            final TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(keystore);
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            return sslContext;
        } catch (Exception e) {
            throw new RuntimeException("Could not set up SSL context", e);
        }
    }

    private static HttpsConfigurator getConfigurator(final SSLContext sslContext) {
        return new HttpsConfigurator(sslContext) {
            @Override
            public void configure(HttpsParameters params) {
                final SSLParameters sslParams = getSSLContext().getDefaultSSLParameters();
                params.setNeedClientAuth(false);
                params.setSSLParameters(sslParams);
            }
        };
    }

    private static BufferedImage loadImage(String imageResourceName) throws IOException {
        // do not let the TrayIcon scale the icons: they won't be smooth
        BufferedImage resizedImage = new BufferedImage(tray.getTrayIconSize().width, tray.
                getTrayIconSize().height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(
                ImageIO.read(Main.class.getResource(imageResourceName))
                        .getScaledInstance(tray.getTrayIconSize().width,
                                tray.getTrayIconSize().height,
                                Image.SCALE_SMOOTH),
                0, 0, tray.getTrayIconSize().width, tray.getTrayIconSize().height, Color.WHITE, null);
        g.dispose();
        return resizedImage;
    }

    public static void configure(Map map) {
        String oldPin = config.getProperty("smartcard.pin");
        for (Object o : map.entrySet()) {
            Map.Entry ent = (Map.Entry) o;
            config.put(ent.getKey(), ent.getValue());
        }

        try (FileOutputStream fos = new FileOutputStream(props)) {
            config.store(fos, "EISVI ayarları");
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, props.getAbsolutePath(), t);
        }

        if (config.getProperty("smartcard.pin") != null && !config.getProperty("smartcard.pin").isEmpty() //                &&                !config.getProperty("smartcard.pin").equals(oldPin)
                ) {
            for (Provider p : Security.getProviders()) {
                String name = p.getName();
                if (name.startsWith("SunPKCS11-")) {
                    if (p instanceof sun.security.pkcs11.SunPKCS11) {
                        try {
                            ((sun.security.pkcs11.SunPKCS11) p).logout();
                        } catch (LoginException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "logout from PKCS11", ex);
                        }
                    }

                    p.clear();
                    Security.removeProvider(name);
                    Logger.getLogger(Main.class.getName()).log(Level.INFO, "Removed " + name);
                }
            }

            for (Provider p : Security.getProviders()) {
                String name = p.getName();
                Logger.getLogger(Main.class.getName()).log(Level.INFO, "Provider " + name);
            }

            try {
                setupSignatureDevice(ProviderLoader.getInstance());
            } catch (Throwable t) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Cannot authenticate using PIN.", t);
                JOptionPane.showMessageDialog(null, "İmza cihazına erişimde sorun var.\n" +
                        "PIN bilgisini doğru girdiğinizden emin olun.\n" +
                        "Çoğu sertifika hizmet sağlayıcısı 3 hatalı PIN girişinden sonra imza cihazını kilitlemektedir.\n" +
                        "Böyle bir durumda sertifika hizmet sağlayıcınızın sağladığı PUK kodu ile kilidi kaldırıp PIN'inizi güncelleyebilirsiniz.",
                        "Hata!", JOptionPane.ERROR_MESSAGE);
            }

            for (Provider p : Security.getProviders()) {
                String name = p.getName();
                Logger.getLogger(Main.class.getName()).log(Level.INFO, "Provider " + name);
            }
        }
    }

}

class EISVIServer implements HttpHandler {

    RemoteSignerImpl theService;

    public EISVIServer() {
        try {
            theService = new RemoteSignerImpl(0);
        } catch (RemoteException ex) {
            Logger.getLogger(EISVIServer.class.getName()).log(Level.SEVERE, "Error creating local signer service", ex);
            JOptionPane.showMessageDialog(null, "Yerel imza hizmetine erişim açmakta sorun var.\n" +
                    "Bilgisayarınızı kapatıp açmayı deneyebilirsiniz.\n" +
                    "\n\nKapatıp açtıktan sonra da çalışmazsa, destek çağrısı açınız.", "Hata!", JOptionPane.ERROR_MESSAGE);
            System.exit(9);
        }
    }

    @Override
    public void handle(HttpExchange he) throws IOException {
        try {
            // determine encoding
            String contentType = he.getRequestHeaders().getFirst("Content-Type");
            String encoding = "ISO-8859-1";
            Map<String, List<String>> headers = new HashMap();
            if (contentType != null) {
                String[] parsed = contentType.split("[;]");
                for (String attr : parsed) {
                    String def = attr.trim();
                    int ix = def.indexOf('=');
                    String name;
                    String value;
                    if (ix < 0) {
                        name = URLDecoder.decode(def, encoding);
                        value = "";
                    } else {
                        name = URLDecoder.decode(def.substring(0, ix), encoding);
                        value = URLDecoder.decode(def.substring(ix + 1), encoding);
                    }
                    List<String> list = headers.get(name);
                    if (list == null) {
                        list = new ArrayList<String>();
                        headers.put(name, list);
                    }
                    list.add(value);
                }
                if (headers.containsKey("charset")) {
                    encoding = headers.get("charset").get(0);
                }
            }

            String queryParams = he.getRequestURI().getQuery();

            // parse the query
            Map<String, List<String>> parameters = new HashMap<String, List<String>>();
            String defs[] = queryParams.split("[&]");
            for (String def : defs) {
                int ix = def.indexOf('=');
                String name;
                String value;
                if (ix < 0) {
                    name = URLDecoder.decode(def, encoding);
                    value = "";
                } else {
                    name = URLDecoder.decode(def.substring(0, ix), encoding);
                    value = URLDecoder.decode(def.substring(ix + 1), encoding);
                }
                List<String> list = parameters.get(name);
                if (list == null) {
                    list = new ArrayList<String>();
                    parameters.put(name, list);
                }
                list.add(value);
            }

            Logger.getLogger(Main.class.getName()).
                    log(Level.INFO, "   headers: " + he.getRequestHeaders().entrySet().toString());
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "parameters: " + parameters.
                    toString());

            he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            he.getResponseHeaders().add("Access-Control-Allow-Credentials", "true");
            he.getResponseHeaders().add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            he.getResponseHeaders().add("Access-Control-Max-Age", "3600");
            he.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");

            if ("check".equalsIgnoreCase(parameters.get("type").get(0))) {
                byte[] signed = "OK".getBytes();
                he.sendResponseHeaders(200, 0);
                OutputStream os = he.getResponseBody();
                if (parameters.containsKey("base64")) {
                    os.write(Base64.getEncoder().encode(signed));
                } else {
                    os.write(signed);
                }
                os.close();
                he.close();

                return;
            }

            String body = null;
            if ("POST".equals(he.getRequestMethod())) {
                InputStream input = he.getRequestBody();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[16384];
                int n;
                do {
                    n = input.read(buffer);
                    if (n > 0) {
                        baos.write(buffer, 0, n);
                    }
                } while (n > 0);

//                String encoded = new String(baos.toByteArray(), "utf-8");
//                String decoded = URLDecoder.decode(encoded, "utf-8");
                MultipartFormdata mpfd = new MultipartFormdata(baos.toByteArray(), parameters.containsKey("base64"));
                Logger.getLogger(Main.class.getName()).log(Level.INFO, "boundary: " + mpfd.getBoundary());

                for (SinglePart sp : mpfd.getParts()) {
                    if (sp.getHeaders().get("Content-Disposition") != null &&
                            sp.getHeaders().get("Content-Disposition").get("filename") != null) {
                        // there's a file, sign it
                        String signedFileName = sp.getHeaders().get("Content-Disposition").get("filename");

                        CertificateSelector cs;
                        if (parameters.containsKey("signerId")) {
                            cs = new SubjectSerialNumberCertSelector(parameters.get("signerId").get(0));
                        } else {
                            he.getResponseHeaders().add("Content-type", "application/json");
                            he.sendResponseHeaders(400, 0);
                            OutputStream os = he.getResponseBody();
                            os.write(("{\"status\":\"error\",\"message\":\"Bad request, no signer ID specified\"}").getBytes(
                                    "utf-8"));

                            os.close();
                            he.close();
                            return;
                        }

                        Logger.getLogger(Main.class.getName()).log(Level.INFO, "Worker: " + RemoteSignerWorkerManager.
                                getInstance().acquireToken(cs));

                        byte[] signed;

                        if ("PAdES".equalsIgnoreCase(parameters.get("type").get(0))) {
                            signed = theService.signRemotePDF(cs, mpfd.getParts().get(0).getFile(), false);
                        } else if ("CAdES".equalsIgnoreCase(parameters.get("type").get(0))) {
                            if (signedFileName.charAt(signedFileName.length() - 1) == '"') {
                                signedFileName = signedFileName.substring(0, signedFileName.length() - 1) + ".p7s\"";
                            } else {
                                signedFileName = signedFileName + ".p7s";
                            }

                            signed = theService.signRemoteBytes(cs, mpfd.getParts().get(0).getFile(), false, "attached:" +
                                    signedFileName);
                        } else if ("XAdES".equalsIgnoreCase(parameters.get("type").get(0))) {
                            XMLSignProfile xmlsp = new XMLSignProfile();
                            xmlsp.setApplication("localsignman");
                            xmlsp.setSignType(SignPropsConstants.XMLSignType.XAdES_BES);
                            signed = theService.signRemoteByteXML(cs, mpfd.getParts().get(0).getFile(), xmlsp);
                        } else {
                            he.getResponseHeaders().add("Content-type", "application/json");
                            he.sendResponseHeaders(400, 0);
                            OutputStream os = he.getResponseBody();
                            os.write(("{\"status\":\"error\",\"message\":\"Bad request, no signature type specified\"}").
                                    getBytes(
                                            "utf-8"));

                            os.close();
                            he.close();
                            return;
                        }

                        he.getResponseHeaders().add("Content-Disposition", "attachment; filename=" + signedFileName);
                        he.getResponseHeaders().add("Content-Type", "application/octet-stream");
                        he.getResponseHeaders().add("Content-Transfer-Encoding", "Binary");
                        he.sendResponseHeaders(200, 0);
                        OutputStream os = he.getResponseBody();
                        if (parameters.containsKey("base64")) {
                            os.write(Base64.getEncoder().encode(signed));
                        } else {
                            os.write(signed);
                        }
                        os.close();
                        he.close();

                        return; // we return the first signed content, HTTP does not allow multiple returns
                    }
                }
            }

            he.getResponseHeaders().add("Content-type", "application/json");
            he.sendResponseHeaders(400, 0);
            OutputStream os = he.getResponseBody();
            os.write(("{\"status\":\"error\",\"data\":\"Bad request, only POST is allowed\"}").getBytes("utf-8"));

            os.close();
            he.close();
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.WARNING, "Web request", t);
            he.getResponseHeaders().add("Content-type", "application/json");
            he.sendResponseHeaders(500, 0);
            OutputStream os = he.getResponseBody();
            os.write(
                    ("{\"status\":\"error\",\"data\":\"Some technical thing failed, the details are in the logs, but here's a peek: " +
                            t.getMessage() + "\"}").getBytes("utf-8"));

            os.close();
            he.close();
        }
    }
}

class MultipartFormdata {

    private byte[] content;
    private String boundary;
    private boolean base64Encoded;

    private List<SinglePart> parts = new ArrayList<>();

    public List<SinglePart> getParts() {
        return parts;
    }

    public byte[] getContent() {
        return content;
    }

    public String getBoundary() {
        return boundary;
    }

    public MultipartFormdata(byte[] streamContent, boolean base64Encoded) {
        this.base64Encoded = base64Encoded;
        content = streamContent;
        try {
            boundary = readBoundary();
            readPartsIn();
        } catch (IOException ex) {
            Logger.getLogger(MultipartFormdata.class.getName()).log(Level.SEVERE, "Error reading multipart boundary.", ex);
        }
    }

    public String readBoundary() throws IOException {
        byte[] boundaryBytes = null;

        for (int i = 0; i < 256; i++) {
            if (content[i] == '\r' && content[i + 1] == '\n') {
                boundaryBytes = new byte[i + 1];
                System.arraycopy(content, 0, boundaryBytes, 0, i + 1);
                break;
            } else if (content[i] == '\n') {
                boundaryBytes = new byte[i];
                System.arraycopy(content, 0, boundaryBytes, 0, i);
                break;
            }
        }

        if (boundaryBytes != null) {
            Logger.getLogger(MultipartFormdata.class.getName()).log(Level.INFO, "boundary computed: '" + new String(
                    boundaryBytes) +
                    "'");
        }

        String bline = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(content))).readLine().trim();
        Logger.getLogger(MultipartFormdata.class.getName()).log(Level.INFO, "boundary read: '" + bline + "'");

        return bline;
    }

    private void readPartsIn() {
        byte[] boundaryBytes = boundary.getBytes();

        List<int[]> partStarts = new ArrayList<>();
        int pos = 0;
        int start, end;

        do {
            start = match(content, pos, boundaryBytes);
            end = -1;
            if (start >= 0) {
                end = match(content, start + boundaryBytes.length, boundaryBytes);
            }

            if (start >= 0) {

                if (end < 0) {
                    end = content.length - 1;
                }

                pos = end + boundaryBytes.length;

                if (content[end - 1] == '\n') {
                    end--;
                }
                if (content[end - 1] == '\r') {
                    end--;
                }
                int[] range = new int[2];
                range[0] = start + boundaryBytes.length;
                range[1] = end;
                partStarts.add(range);

            }
        } while (start >= 0 && pos < content.length);

        Logger.getLogger(MultipartFormdata.class.getName()).log(Level.INFO, "parts ranges: '" + Arrays.deepToString(partStarts.
                toArray()) + "'");

        for (int[] range : partStarts) {
            int b = range[0];
            int e = range[1];

            byte[] part;

            part = new byte[e - b];
            System.arraycopy(content, b, part, 0, part.length);

            SinglePart sp = new SinglePart(part, base64Encoded);
            parts.add(sp);

//      try {
//        FileOutputStream fos = new FileOutputStream("C:/Users/Ferhat/Desktop/deneme.out");
//        fos.write(sp.getFile());
//        fos.close();
//      } catch (Throwable t) {
//        t.printStackTrace(System.err);
//      }
            Logger.getLogger(MultipartFormdata.class.getName()).log(Level.INFO, "part headers: " + sp.getHeaders().toString());
        }
    }

    public static int match(byte[] content, int pos, byte[] sought) {
        for (int i = pos; i < content.length - sought.length + 1; i++) {
            boolean found = true;
            for (int j = 0; j < sought.length; ++j) {
                if (content[i + j] != sought[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }
}

class SinglePart {

    private Map<String, Map<String, String>> headers = new HashMap<>();
    private byte[] file;
    private boolean base64Encoded = false;

    SinglePart(byte[] part, boolean base64Encoded) {
        this.base64Encoded = base64Encoded;
        
        String LFLF = "\n\n";
        String CRLFCRLF = "\r\n\r\n";
        int skip = CRLFCRLF.getBytes().length;
        int emptyLine = MultipartFormdata.match(part, 0, CRLFCRLF.getBytes());
        if (emptyLine < 0) {
            skip = LFLF.getBytes().length;
            emptyLine = MultipartFormdata.match(part, 0, LFLF.getBytes());
        }

        if (emptyLine < 0) {
            file = new byte[part.length];
            System.arraycopy(part, 0, file, 0, part.length);
            return;
        }

        file = new byte[part.length - emptyLine - skip];
        System.arraycopy(part, emptyLine + skip, file, 0, file.length);
        
        if (base64Encoded) {
            file = Base64.getDecoder().decode(new String(file).trim());
        }

        byte[] headerBytes = new byte[emptyLine];
        System.arraycopy(part, 0, headerBytes, 0, emptyLine);

        ByteArrayInputStream bais = new ByteArrayInputStream(headerBytes);
        BufferedReader br = new BufferedReader(new InputStreamReader(bais));
        String line = null;
        do {
            try {
                line = br.readLine();
            } catch (IOException ex) {
                Logger.getLogger(SinglePart.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (line != null && line.length() > 0) {
                int colonIndex = line.indexOf(':');
                String headerName = line;
                if (colonIndex > 0) {
                    headerName = line.substring(0, colonIndex);

                    String[] partHeaders = line.substring(colonIndex + 1).split("[;]");
                    for (String def : partHeaders) {
                        def = def.trim();
                        int ix = def.indexOf('=');
                        String attrName;
                        String attrValue;
                        if (ix < 0) {
                            attrName = def;
                            attrValue = "";
                        } else {
                            attrName = def.substring(0, ix);
                            attrValue = def.substring(ix + 1);
                        }
                        Map<String, String> map = headers.get(headerName);
                        if (map == null) {
                            map = new HashMap<String, String>();
                            headers.put(headerName, map);
                        }
                        map.put(attrName, attrValue);
                    }
                }
            }
        } while (line != null);

    }

    public Map<String, Map<String, String>> getHeaders() {
        return headers;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

}
