/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.eisvi;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import tr.com.cs.atr2prov.ProviderLoader;

/**
 *
 * @author ferhats
 */
public class Configurator extends javax.swing.JPanel {

  /**
   * Creates new form Configurator
   */
  public Configurator() {
    initComponents();
    // TODO Fill in dialog contents from Main
    serviceEndpoint.setText(Main.config.getProperty("service.endpoint"));
    imzaciBilgileri.setText(ProviderLoader.getInstance().getCert().getSubjectDN().getName());
    PIN.setText(Main.config.getProperty("smartcard.pin"));
    tlsHttps.setSelected(Boolean.parseBoolean(Main.config.getProperty("https.tls")));
    serverDurumu.setText(Main.isTlsServer() ? "Şu an: şifreli" : "Şu an: şifresiz");
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of
   * this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jTabbedPane1 = new javax.swing.JTabbedPane();
    jPanel1 = new javax.swing.JPanel();
    jLabel2 = new javax.swing.JLabel();
    serviceEndpoint = new javax.swing.JTextField();
    jPanel2 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    PIN = new javax.swing.JPasswordField();
    imzaciBilgileri = new javax.swing.JLabel();
    jPanel3 = new javax.swing.JPanel();
    tlsHttps = new javax.swing.JCheckBox();
    jLabel3 = new javax.swing.JLabel();
    serverDurumu = new javax.swing.JLabel();
    jButton1 = new javax.swing.JButton();
    jButton2 = new javax.swing.JButton();

    jLabel2.setText("URL:");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jLabel2)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(serviceEndpoint, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
        .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        .addContainerGap(64, Short.MAX_VALUE)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2)
          .addComponent(serviceEndpoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(66, 66, 66))
    );

    jTabbedPane1.addTab("Servis bilgileri", jPanel1);

    jLabel1.setText("P I N:");

    PIN.setPreferredSize(new java.awt.Dimension(64, 19));

    imzaciBilgileri.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    imzaciBilgileri.setText("İmzacı DN");
    imzaciBilgileri.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    imzaciBilgileri.setName(""); // NOI18N

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(200, 200, 200)
        .addComponent(jLabel1)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(PIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(202, Short.MAX_VALUE))
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(imzaciBilgileri, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(imzaciBilgileri, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel1)
          .addComponent(PIN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(102, 102, 102))
    );

    jTabbedPane1.addTab("İmzacı bilgileri", jPanel2);

    jPanel3.setName(""); // NOI18N

    tlsHttps.setText("İstekleri https (TLS) şifreleme ile koru*");
    tlsHttps.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tlsHttpsActionPerformed(evt);
      }
    });

    jLabel3.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
    jLabel3.setText("* Uygulama yeniden başlatıldığında devreye girer.");

    serverDurumu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    serverDurumu.setText("Mevcut güvenlik ayarı: açık / kapalı");

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
        .addContainerGap(117, Short.MAX_VALUE)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addGap(14, 14, 14)
            .addComponent(tlsHttps))
          .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(serverDurumu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addGap(109, 109, 109))
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addGap(28, 28, 28)
        .addComponent(tlsHttps)
        .addGap(18, 18, 18)
        .addComponent(jLabel3)
        .addGap(18, 18, 18)
        .addComponent(serverDurumu)
        .addContainerGap(34, Short.MAX_VALUE))
    );

    jTabbedPane1.addTab("Yerel servis güvenliği", jPanel3);

    jButton1.setText("Tamam");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    jButton2.setText("İptal");
    jButton2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton2ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jTabbedPane1)
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(jButton2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButton1)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButton1)
          .addComponent(jButton2))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
  }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      // TODO Main.configure() with dialog contents and...
      Map map = new HashMap();
      map.put("service.endpoint", serviceEndpoint.getText());
      map.put("signer.dn", imzaciBilgileri.getText());
      map.put("smartcard.pin", new String(PIN.getPassword()));
      map.put("https.tls", tlsHttps.isSelected() ? "true" : "false");
      Main.configure(map);
      Main.configureDialog.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
      // Discard dialog contents and...
      Main.configureDialog.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

  private void tlsHttpsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tlsHttpsActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_tlsHttpsActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPasswordField PIN;
  private javax.swing.JLabel imzaciBilgileri;
  private javax.swing.JButton jButton1;
  private javax.swing.JButton jButton2;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JTabbedPane jTabbedPane1;
  private javax.swing.JLabel serverDurumu;
  private javax.swing.JTextField serviceEndpoint;
  private javax.swing.JCheckBox tlsHttps;
  // End of variables declaration//GEN-END:variables
}
